import { Button, Flex, FormControl, Input } from "@chakra-ui/react"
import { useState } from "react"
import { useDispatch } from "react-redux";
import { addTodo } from "../redux/action";


export const AddTodo = () => {
    const dispatch = useDispatch();
    const [value, setValue] = useState(' ');

    const handleSubmit = e => {
        e.preventDefault();
        dispatch(addTodo(value));
        setValue(' ');
    }

    const handleInput = e => {
        setValue(e.target.value);
    }
  return (
    <form onSubmit={handleSubmit}>
        <Flex>
            <FormControl>
                <Input 
                value={value}
                type='text'
                onChange={handleInput}
                borderTopRightRadius={0}
                borderBottomRightRadius={0}
                />
            </FormControl>
            <Button
            borderTopLeftRadius={0}
            borderBottomLeftRadius={0}
            colorScheme='teal'
            type="submit"
            disabled={!value}
            > Add My Todo </Button>
        </Flex>
    </form>
  )
}
