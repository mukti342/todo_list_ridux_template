import { Container, Tab, TabList, Tabs } from '@chakra-ui/react'
import { useDispatch } from 'react-redux'
import { VISIBILITY_FILTER } from '../constants'
import { setFilter } from '../redux/action';

export const VisibilityFilter = () => {
  const dispatch = useDispatch();
  return (
    <Container centerContent>
        <Tabs>
            <TabList>
              {Object.keys(VISIBILITY_FILTER).map(filterkey =>{
                  const currenFilter = VISIBILITY_FILTER[filterkey];
                  return (
                      <Tab key={`visibility-filter-${currenFilter}`}
                        onClick={()=> dispatch(setFilter(currenFilter))}
                      >
                          {currenFilter}
                      </Tab>
                  )
              })}
            </TabList>
        </Tabs>
    </Container>
  )
}
